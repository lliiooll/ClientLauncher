package cn.lliiooll.launcher.load;

import cn.lliiooll.launcher.Back;
import cn.lliiooll.launcher.BackInterface;
import cn.lliiooll.launcher.server.Server;
import cn.lliiooll.launcher.window.Window;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.*;
import java.net.Socket;

public class Download implements Runnable{

    private final Logger log = LogManager.getLogger(this.getClass());
    public static long max;
    public static int maxbak;
    public static int maxmb;

    @Override
    public void run() {
        try {
            File fp = new File("tmp//download");
            if(!fp.exists()){
                fp.mkdirs();
            }

            log.info("建立连接");
            Socket s = new Socket(Server.getHost(), Server.getPort());
            DataInputStream in = new DataInputStream(s.getInputStream());
            DataOutputStream out = new DataOutputStream(s.getOutputStream());
            log.info("开始下载");
            Window.ujpb.setString("开始下载");
            Window.ujpb.setIndeterminate(false);
            File file = new File("tmp//download//update.dat");
            if(!file.exists()){
                file.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(file);
            //LogOut.info("正在下载");
            byte[] b = new byte[1073741824];
            int i = 0;
            out.writeInt(1);
            max = in.readLong();
            long mb = max;
            maxbak = (int) (max/1024);
            max = max/1024;
            max = max/1024;
            maxmb = (int) max;
            Window.ujpb.setMaximum((int)mb);
            Window.ujpb.setMinimum(1);
            while((i = in.read(b,0,b.length)) != -1) {
                fos.write(b, 0, i);
                log.info("已下载" + file.length() + "字节/共" + mb + "字节");
                Window.ujpb.setValue((int)file.length());
                Window.ujpb.setString("已下载" + String.format("%.2f", (double)file.length()/1024/1024) + "/" + (double)maxmb + "MB");
            }
            in.close();
            fos.flush();
            out.close();
            s.close();
            //LogOut.info("下载完成");
            log.info("下载完成");
            BackInterface b1 = new Back();
            b1.setType("download",true);
        } catch (IOException e) {
            log.error("I/O");
            e.printStackTrace();
            System.exit(0);
        }
    }
}
