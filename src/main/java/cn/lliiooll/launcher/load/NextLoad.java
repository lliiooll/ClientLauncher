package cn.lliiooll.launcher.load;

import cn.lliiooll.launcher.Back;
import cn.lliiooll.launcher.BackInterface;
import cn.lliiooll.launcher.server.Pm;
import cn.lliiooll.launcher.update.FileSet;
import cn.lliiooll.launcher.window.MainWindow;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NextLoad implements Runnable{

    private final Logger log = LogManager.getLogger(this.getClass());

    @Override
    public void run() {
        log.info("准备获取公告...");
        Thread t = new Thread(new Pm(),"PublicMsg");
        t.start();
        BackInterface b = new Back();
        b.addType("publicmsg",false);
        log.info("等待公告线程结束...");
        while(!b.getType("publicmsg")){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error("发生一个错误");
                log.error(e.getMessage());
                System.exit(0);
            }
        }
        log.info("加载公告...");
        MainWindow.l.setText(FileSet.readFile("pm.dat","tmp"));
        log.info("完成加载，退出线程...");
        b.setType("load_2",true);
        b.delType("publicmsg");
    }
}
