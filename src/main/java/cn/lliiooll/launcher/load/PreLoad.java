package cn.lliiooll.launcher.load;

import cn.lliiooll.launcher.Back;
import cn.lliiooll.launcher.BackInterface;
import cn.lliiooll.launcher.Windows;
import cn.lliiooll.launcher.server.Server;
import cn.lliiooll.launcher.update.FileSet;
import cn.lliiooll.launcher.update.Update;
import cn.lliiooll.launcher.update.Zip;
import cn.lliiooll.launcher.window.LoadWindow;
import cn.lliiooll.launcher.window.Window;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class PreLoad implements Runnable{

    private static final Logger log = LogManager.getLogger(PreLoad.class);

    @Override
    public void run() {
        log.info("开始加载...");
        //在此放入加载代码
        File f_tmp = new File("tmp");
        if(!f_tmp.exists()){
            f_tmp.mkdirs();
            f_tmp.mkdir();
        }
        log.info("开始检测服务器...");
        BackInterface b = new Back();
        b.addType("server",false);
        Thread t = new Thread(new Server(),"Server");
        t.start();
        log.info("等待服务器线程完成工作...");
        Windows.setWindow("load");
        Thread t11111111 = new Thread(new Windows(),"Windows");
        t11111111.start();
        while(!b.getType("server")){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error("发生一个错误");
                log.error(e.getMessage());
                System.exit(0);
            }
        }
        LoadWindow.guw.dispose();
        Windows.setWindow("getupdate");
        Thread t1111 = new Thread(new Windows(),"Windows");
        t1111.start();
        b.addType("update",false);
        Thread t1 = new Thread(new Update(),"Update");
        t1.start();
        log.info("等待更新线程完成工作...");
        while(!b.getType("update")){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error("发生一个错误");
                log.error(e.getMessage());
                System.exit(0);
            }
        }
        Window.guw.dispose();
        if(Update.getUpdate()){
            Windows.setWindow("update");
            Window.ujpb.setIndeterminate(false);
            Thread t11111 = new Thread(new Windows(),"Windows");
            t11111.start();
            log.info("检测到更新,下载更新");
            b.addType("download",false);
            Thread t11 = new Thread(new Download(),"Download");
            t11.start();
            while(!b.getType("download")){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    log.error("发生一个错误");
                    log.error(e.getMessage());
                    System.exit(0);
                }
            }
            log.info("下载完毕.准备解压");
            b.addType("unzip",false);

            Zip.f = new File("tmp//download//update.dat");
            Thread t111 = new Thread(new Zip(),"UnZip");
            t111.start();
            while(!b.getType("unzip")){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    log.error("发生一个错误");
                    log.error(e.getMessage());
                    System.exit(0);
                }
            }
            Window.ujpb.setString("开始复制文件...");
            Window.ujpb.setIndeterminate(true);
            File f = new File("");
            try {
                FileSet.copyDir("tmp//unzip",f.getCanonicalPath());
            } catch (IOException e) {
                log.error("发生一个错误");
                log.error(e.getMessage());
                System.exit(0);
            }
            Window.ujpb.setString("完成");
        }else{
            log.info("已经是最新版本");
        }
        b.setType("load",true);
        b.delType("unzip");
        b.delType("server");
        b.delType("update");
        //Window.uw.dispose();
    }
}
