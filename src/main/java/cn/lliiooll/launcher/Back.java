package cn.lliiooll.launcher;

import java.util.HashMap;
import java.util.Map;

public class Back implements BackInterface{

    private static Map<String,Boolean> m = new HashMap<String,Boolean>();

    @Override
    public void addType(String name, boolean type) {
        m.put(name,type);
    }

    @Override
    public void setType(String name,boolean type) {
        m.put(name,type);
    }

    @Override
    public boolean getType(String name) {
        return m.get(name);
    }

    @Override
    public void delType(String name) {
        m.remove(name);
    }
}
