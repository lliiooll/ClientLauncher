package cn.lliiooll.launcher.server;

import cn.lliiooll.launcher.Back;
import cn.lliiooll.launcher.BackInterface;
import cn.lliiooll.launcher.update.FileSet;
import cn.lliiooll.launcher.window.Window;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class Pm implements Runnable{

    private final Logger log = LogManager.getLogger(this.getClass());

    @Override
    public void run() {
        log.info("获取公告...");
        try {
            Socket s = new Socket(Server.getHost(),Server.getPort());
            DataOutputStream dos = new DataOutputStream(s.getOutputStream());
            DataInputStream in = new DataInputStream(s.getInputStream());
            dos.writeInt(3);
            byte[] b = new byte[1024];
            int i = 0;
            File f = new File("tmp//pm.dat");
            if(!f.exists()){
                f.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(f);
            while((i = in.read(b,0,b.length)) != -1) {
                fos.write(b, 0, i);
                log.info("已下载" + f.length() + "字节");
            }
            s.close();
            dos.close();
            in.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BackInterface b = new Back();
        b.setType("publicmsg",true);
        log.info("完毕");
    }
}
