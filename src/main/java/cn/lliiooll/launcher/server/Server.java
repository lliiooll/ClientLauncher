package cn.lliiooll.launcher.server;

import cn.lliiooll.launcher.Back;
import cn.lliiooll.launcher.BackInterface;
import cn.lliiooll.launcher.load.Download;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class Server implements Runnable{

    public static boolean ok = false;
    private final Logger log = LogManager.getLogger(this.getClass());

    public static String getHost(){
        return "lliiooll.tpddns.cn";
    }

    public static int getPort(){
        return 2333;
    }

    @Override
    public void run() {
        try {
            log.info("服务器连接中...");
            Socket s = new Socket(this.getHost(), this.getPort());
            OutputStream os = s.getOutputStream();
            InputStream is = s.getInputStream();
            DataOutputStream dos = new DataOutputStream(os);
            DataInputStream dis = new DataInputStream(is);
            dos.writeInt(0);
            int i = dis.readInt();
            if(i == 0){
                log.info("服务器连接成功");
            }

        } catch (IOException e) {
            log.error("服务器连接失败");
            log.error(e.getMessage());
            System.exit(0);
        }
        BackInterface b = new Back();
        b.setType("server",true);

    }
}
