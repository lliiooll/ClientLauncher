package cn.lliiooll.launcher.window;

import cn.lliiooll.launcher.update.Update;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class Window extends JFrame{
	
	public static JProgressBar ujpb = new JProgressBar();
	public static JLabel ulabel = new JLabel();
	public static JLabel gulabel = new JLabel();
	public static JFrame guw;
	public static JFrame uw;
	
	public static void updateWindow() {
		uw = new JFrame("神秘部落更新程序");
		uw.setSize(500, 200);
		uw.setLayout(null);
		uw.setVisible(true);
		Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
		uw.setLocation(screenSize.width/3,screenSize.height/3);
		//jpb.setLocation(w1.getHeight()/2, w1.getWidth()/2);
		ujpb.setLocation(20, 130);
		ujpb.setSize(450, 30);
		ujpb.setString("寻找服务器");
		ujpb.setIndeterminate(false);
		ujpb.setStringPainted(true);
		ulabel.setText("更新中，请耐心等待..");
		ulabel.setLocation(180, 10);
		ulabel.setSize(200, 30);
		uw.setDefaultCloseOperation(3);
		uw.add(ulabel);
		uw.add(ujpb);
		uw.setResizable(false);
		//Update.update();
	}
	
	public static void getupdateWindow() {
		guw = new JFrame("神秘部落更新程序");
		guw.setSize(500, 200);
		guw.setLayout(null);
		guw.setVisible(true);
		Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
		guw.setLocation(screenSize.width/3,screenSize.height/3);
		gulabel.setText("获取更新中,请稍后.");
		gulabel.setLocation(180, 10);
		gulabel.setSize(200, 30);
		guw.setDefaultCloseOperation(3);
		JButton b = new JButton();
		guw.add(gulabel);
	}
	
	public static void updateCompeleteWindow() {
		JFrame w1 = new JFrame("神秘部落启动器");
		JLabel label = new JLabel();
		JButton b = new JButton();
		w1.setSize(500, 200);
		w1.setLayout(null);
		w1.setVisible(true);
		Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
		w1.setLocation(screenSize.width/3,screenSize.height/3);
		w1.setResizable(false);
		w1.setDefaultCloseOperation(3);
		label.setText("更新完毕!点击下方按钮关闭程序");
		label.setLocation(180, 10);
		label.setSize(200, 30);
		b.setText("关闭");
		b.setBounds(190, 100, 80, 30);
		b.setActionCommand("reboot");
		b.addActionListener(new ButtonListener());
		w1.add(b);
		w1.add(label);
	}
	
}
