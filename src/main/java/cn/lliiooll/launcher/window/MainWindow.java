package cn.lliiooll.launcher.window;

import javax.swing.*;
import java.awt.*;

public class MainWindow {
	
	public static JFrame w = new JFrame();
	public static JTextArea l = new JTextArea();
	public static JTextArea chats = new JTextArea();
	public static JLabel ll = new JLabel();
	public static JTextField pt = new JTextField();
	public static JTextField ut = new JTextField();
	public static JTextField chatf = new JTextField();
	public static JButton lb = new JButton();

	public static void mw(){
		Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
		w.setTitle("神秘部落启动器");
		w.setSize(1000, 600);
		w.setLayout(null);
		w.setLocation(screenSize.width/4-10,screenSize.height/4-30);
		w.setResizable(false);
		w.setDefaultCloseOperation(3);
		w.setBackground(Color.BLACK);
		//==================================================================
		JScrollPane sp = new JScrollPane(l);
		//l.setText(PublicMsg.getPM());
		l.setEnabled(false);
		l.setLocation(w.getWidth()/6-130,w.getHeight()/5-80);
		l.setSize(w.getHeight()-20,w.getWidth()/4);
		l.setBackground(Color.WHITE);
		l.setWrapStyleWord(true);
		l.setLineWrap(true);
		sp.setSize(w.getHeight()-20,w.getWidth()/4);
		sp.setLocation(w.getWidth()/6-130,w.getHeight()/5-80);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED); 
		//==================================================================
		ut.setText("用户名");
		pt.setText("密码");
		ut.setLocation(w.getWidth()-250,w.getHeight()/4);
		pt.setLocation(w.getWidth()-250,(int) (w.getHeight()/3));
		ut.setSize(100*2-20,30);
		pt.setSize(100*2-20,30);
		//==================================================================
		ll.setText("神秘部落");
		ll.setLocation(w.getWidth()-200,w.getHeight()/4-80);
		ll.setSize(100,100);
		ll.setBackground(Color.WHITE);
		//==================================================================
		lb.setText("登陆");
		lb.setLocation(w.getWidth()-220,(int) (w.getHeight()/2.2));
		lb.setSize(100,50);
		lb.setActionCommand("login");
		lb.addActionListener(new ButtonListener());
		//==================================================================
		
		JScrollPane chat = new JScrollPane(chats);
		chats.setEnabled(false);
		chats.setLocation(w.getWidth()/6-130,w.getHeight()-300);
		chats.setSize(w.getHeight()-20,w.getWidth()/6);
		chats.setBackground(Color.WHITE);
		chatf.setLocation(w.getWidth()/6-130,w.getHeight()-120);
		chatf.setSize(w.getHeight()-20,w.getWidth()/10-70);
		chatf.addActionListener(new TextListener());
		chat.setSize(w.getHeight()-20,w.getWidth()/6);
		chat.setLocation(w.getWidth()/6-130,w.getHeight()-300);
		chat.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED); 
		//==================================================================
		w.add(sp);
		w.add(chat);
		w.add(chatf);
		w.add(ut);
		w.add(pt);
		w.add(ll);
		//w.add(l);
		w.add(lb);
		w.setVisible(true);
	}
	
}
