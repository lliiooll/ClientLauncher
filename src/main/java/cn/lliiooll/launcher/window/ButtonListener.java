package cn.lliiooll.launcher.window;

import cn.lliiooll.launcher.update.FileSet;
import cn.lliiooll.launcher.update.Update;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class ButtonListener implements ActionListener {

	@SuppressWarnings("unused")
	@Override
	public void actionPerformed(ActionEvent e) {
		if("update".equals(e.getActionCommand())) {
			Window.guw.dispose();
			Window.updateWindow();
			//Update.update();
		}
		if("exit".equals(e.getActionCommand())) {
			Window.guw.dispose();
			System.exit(0);
		}
		if("reboot".equals(e.getActionCommand())) {
            Runtime runtime = Runtime.getRuntime();
            try {
				Process process = runtime.exec("libs//Del.bat");
			} catch (IOException e1) {
				// TODO 自动生成的 catch 块
				e1.printStackTrace();
			}
			System.exit(0);

		}
		if("login".equals(e.getActionCommand())) {
			MainWindow.lb.setEnabled(false);
			if(MainWindow.pt.getText().equals("123")) {
				if(MainWindow.ut.getText().equals("123")) {
					JOptionPane.showMessageDialog(null,"密码和用户名正确", "信息",JOptionPane.INFORMATION_MESSAGE); 
					MainWindow.lb.setEnabled(true);
				}else {
					JOptionPane.showMessageDialog(null,"用户名或密码错误", "错误",JOptionPane.ERROR_MESSAGE); 
					MainWindow.lb.setEnabled(true);
				}
			}else {
				JOptionPane.showMessageDialog(null,"用户名或密码错误", "错误",JOptionPane.ERROR_MESSAGE);
				MainWindow.lb.setEnabled(true);
			}
			
			
		}
		
		
	}

}
