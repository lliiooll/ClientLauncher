package cn.lliiooll.launcher.window;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

public class LoadWindow {
	
	private static JProgressBar ujpb = new JProgressBar();
	//public static JLabel ulabel = new JLabel();
	private static JLabel label = new JLabel();
	public static JFrame guw;

	public LoadWindow() {
		guw = new JFrame("神秘部落更新程序");
		guw.setSize(500, 200);
		guw.setLayout(null);
		guw.setVisible(true);
		Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
		guw.setLocation(screenSize.width/3,screenSize.height/3);
		//jpb.setLocation(w1.getHeight()/2, w1.getWidth()/2);
		ujpb.setIndeterminate(true);
		ujpb.setStringPainted(true);
		ujpb.setLocation(20, 130);
		ujpb.setSize(450, 30);
		ujpb.setString("寻找服务器");
		label.setText("寻找服务器中，请耐心等待..");
		label.setLocation(180, 10);
		label.setSize(200, 30);
		guw.setResizable(false);
		guw.setDefaultCloseOperation(3);
		guw.add(label);
		guw.add(ujpb);
		//Update.getUpdate();
	}
}
