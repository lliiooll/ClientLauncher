package cn.lliiooll.launcher.update;

import cn.lliiooll.launcher.load.PreLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class FileSet {

	public static String npath;
	private static final Logger log = LogManager.getLogger(FileSet.class);

	public static void createFile(String filename, String path) {
		File f = new File(path);//设置新的量
		
		if(f.exists()) {//判断文件夹是否存在
			//存在，不做任何动作
		}else {//否则
			f.mkdir();//创建文件夹
		}
		
		File file = new File(path,filename);//设置新的量
	
		if(file.exists()) {//判断文件是否存在
			
		}else {
			try {//这里进行IO操作
				file.createNewFile();//成功，则创建文件
			} catch (IOException e) {
				//失败，则打印出错误
				e.printStackTrace();
			}
		}
		log.info("文件创建完成");
		//print(info + "文件创建完成");
	}
	
	@SuppressWarnings("resource")
	public static void writeFile(String text, String filename, String path) {
		File file = new File(path,filename);
		if(!(file.exists())) {
			log.warn("文件不存在");
			//print(error + "文件不存在");
			log.info("创建文件");
			//print(info + "创建文件");
			createFile(filename, path);
			try {
				//成功，则设置下面的量
				PrintStream ps = new PrintStream(new FileOutputStream(file));
				ps.println(text);//在这里写入文件
				log.info("写入完成");
				} catch (FileNotFoundException e) {
				// TODO 自动生成的 catch 块
				//失败，抛错
				e.printStackTrace();
			}
		}else {
			try {
				//成功，则设置下面的量
				PrintStream ps = new PrintStream(new FileOutputStream(file));
				ps.println(text);//在这里写入文件
				log.info("写入完成");
				} catch (FileNotFoundException e) {
				// TODO 自动生成的 catch 块
				//失败，抛错
				e.printStackTrace();
			}
		}
	}
	
	public static String readFile(String filename, String filepath) {
		File f = new File(filepath + "\\" + filename);   
		if(f.exists()) {
	        byte b[] = new byte[1024];   
	        int len = 0;   
	        int temp= 0;          //所有读取的内容都使用temp接收   
	        try {
	        	InputStream in = new FileInputStream(f);
				while((temp=in.read())!=-1){    //当没有读取完时，继续读取   
				    b[len]=(byte)temp;   
				    len++;   
				}
				in.close(); 
			} catch (IOException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
			return new String(b,0,len);
		}else {
			System.out.println("没有找到文件");
			return null;
		}
	}
	
	public static void copyFile(String oldpath,String newpath, String newname){
		log.info("开始复制文件" + oldpath + "到" + newpath + newname);
		File fold = new File(oldpath);   
		File fnew = new File(newpath);
		File fnewname = new File(newpath + "\\",newname);
		if(!(fold.exists())) {
			System.out.println("源文件不存在");
		}else {
			fnew.mkdirs();
			try {
				fnewname.createNewFile();
			} catch (IOException e1) {
				// TODO 自动生成的 catch 块
				e1.printStackTrace();
			}
				byte[] b = new byte[1024];
				int n = 0;
				try {
					FileInputStream in = new FileInputStream(fold);
					FileOutputStream out = new FileOutputStream(newpath + "\\" + newname);
					while((n = in.read(b)) != -1) {
						out.write(b, 0, n);
					}
					out.close();
					in.close();
				} catch (Exception e) {
					// TODO 自动生成的 catch 块
					e.printStackTrace();
				}
		}
	}
	
	public static void shearFile(String oldpath,String newpath, String newname){
		File fold = new File(oldpath);   
		File fnew = new File(newpath);
		File fnewname = new File(newpath + "\\",newname);
		if(!(fold.exists())) {
			System.out.println("源文件不存在");
		}else {
			fnew.mkdirs();
			try {
				fnewname.createNewFile();
			} catch (IOException e1) {
				// TODO 自动生成的 catch 块
				e1.printStackTrace();
			}
				byte[] b = new byte[1024];
				int n = 0;
				try {
					FileInputStream in = new FileInputStream(fold);
					FileOutputStream out = new FileOutputStream(newpath + "\\" + newname);
					while((n = in.read(b)) != -1) {
						out.write(b, 0, n);
					}
					out.close();
					in.close();
					fold.delete();
					System.out.println("剪切完成");
				} catch (Exception e) {
					// TODO 自动生成的 catch 块
					e.printStackTrace();
				}
		}
	}
	
	public static void copyDir(String oldpath,String newpath) {

		log.info("开始复制" + oldpath + "到" + newpath);
		File f = new File(oldpath);
		File[] fs = f.listFiles();
		for (int i = 0; i < fs.length; i++) {
			if (fs[i].isDirectory()) {
				copyDir(oldpath + "\\" + fs[i].getName(), newpath + "\\" + fs[i].getName());
			} else {
				copyFile(fs[i].getAbsolutePath(), newpath + "\\", fs[i].getName());
			}
		}

	}

	public static boolean delDir(File file) {
        if (!file.exists()) {
            return false;
        }

        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                delDir(f);
            }
        }
        return file.delete();
    }
	
	
}
