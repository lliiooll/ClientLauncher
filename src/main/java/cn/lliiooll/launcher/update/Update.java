package cn.lliiooll.launcher.update;

import cn.lliiooll.launcher.Back;
import cn.lliiooll.launcher.BackInterface;
import cn.lliiooll.launcher.server.Server;
import com.alibaba.fastjson.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

public class Update implements Runnable{

	private static double ver = 0.0;
	private static boolean update = false;

	private final Logger log = LogManager.getLogger(this.getClass());

	public static boolean getUpdate(){
		return update;
	}

	public static double getVersion(){
		return ver;
	}

	@Override
	public void run() {

		try {
			Socket s = new Socket(Server.getHost(),Server.getPort());
			DataInputStream dis = new DataInputStream(s.getInputStream());
			DataOutputStream dos = new DataOutputStream(s.getOutputStream());
			log.info("开始检测更新...");
			dos.writeInt(2);
			double ver = dis.readDouble();
			log.info("最新版本" + ver + "版本");
			File f = new File("tmp//version.json");
			JSONObject j = new JSONObject();
			if(!f.exists()){
				f.createNewFile();
				j.put("version",0.0);
				FileSet.writeFile(j.toString(),"version.json","tmp");
			}
			JSONObject o = JSONObject.parseObject(FileSet.readFile("version.json","tmp"));
			this.ver = o.getDouble("version");
			log.debug("目前版本" + this.ver + "版本");
			if(this.ver != ver){
				update = true;
				this.ver = ver;
				j.put("version",ver);
				FileSet.writeFile(j.toString(),"version.json","tmp");
			}
			s.close();
			dis.close();
			dos.close();
			BackInterface b = new Back();
			b.setType("update",true);
		} catch (IOException e) {
			log.error("I/O");
			e.printStackTrace();
			System.exit(0);
		}
	}
}
