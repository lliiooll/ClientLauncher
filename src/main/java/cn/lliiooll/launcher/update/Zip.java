package cn.lliiooll.launcher.update;

import cn.lliiooll.launcher.Back;
import cn.lliiooll.launcher.BackInterface;
import cn.lliiooll.launcher.window.Window;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Zip implements Runnable{
	
	public static int size;
	public static File f;
	private final Logger log = LogManager.getLogger(this.getClass());

	@Override
	public void run() {
		Window.ujpb.setValue(0);
		try {
			@SuppressWarnings("resource")
			ZipFile zip = new ZipFile(f, Charset.forName("GBK"));
			Window.ujpb.setMaximum(zip.size());
			Window.ujpb.setMinimum(1);
			//String name = zip.getName().substring(zip.getName().lastIndexOf('\\')+1, zip.getName().lastIndexOf('.'));
			File filepath = new File("tmp//unzip");
			if(!(filepath.exists())) {
				filepath.mkdirs();
			}
			//LogOut.info("开始解压");
			Window.ujpb.setString("开始解压");
			//Window.ujpb.setIndeterminate(true);
			for(Enumeration<? extends ZipEntry> entries = zip.entries(); entries.hasMoreElements();) {
				ZipEntry entry = entries.nextElement();
				String zipEntryName = entry.getName();
				InputStream is = zip.getInputStream(entry);
				String outPath = ("tmp//unzip"+"/"+ zipEntryName).replaceAll("\\*", "/");
				File file1 = new File(outPath.substring(0, outPath.lastIndexOf('/')));
				if(!(file1.exists())) {
					file1.mkdirs();
				}
				if(new File(outPath).isDirectory()) {
					continue;
				}
				FileOutputStream fos = new FileOutputStream(outPath);
				byte[] b = new byte[1024];
				int i;

				//File f = new File(path);
				//int sizebak = size;
				//getFile(f);
				size++;
				Window.ujpb.setValue(size);
				//Window.ujpb.setString("已解压" + size + "/" + zip.size() + "个文件");
				log.info("已解压" + size + "/" + zip.size() + "个文件");
				Window.ujpb.setString("已解压" + size + "/" + zip.size() + "个文件");

				while((i = is.read(b)) > 0) {
					fos.write(b,0,i);
				}
				is.close();
				fos.close();
			}
			if(size < zip.size()) {
				size = zip.size();
			}
			log.info("解压完成");
			//Window.ujpb.setString("解压完成");
			//FileSet.update();
			//Window.uw.dispose();
			//Window.updateCompeleteWindow();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		BackInterface b1 = new Back();
		b1.setType("unzip",true);
	}
	
    /*public static void getFile(File f){
        if(f!=null){
            if(f.isDirectory()){
                File[] fileArray=f.listFiles();
                if(fileArray!=null){
                    for (int i = 0; i < fileArray.length; i++) {
                        //递归调用
                        getFile(fileArray[i]);
                    }
                }
            }
            else{
                size++;
            }
        }
    }*/
}
