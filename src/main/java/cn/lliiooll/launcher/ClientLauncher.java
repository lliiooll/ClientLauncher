package cn.lliiooll.launcher;

import cn.lliiooll.launcher.load.NextLoad;
import cn.lliiooll.launcher.load.PreLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ClientLauncher {

	/*
	1、写完聊天(chat)系统
	2、写完启动(minecraft)系统
	3、写完加密(encryption)系统
	4、修bug
	 */
	
	public static int i = 5;

	public static void main(String[] args) {
		//客户端启动
		final Logger log = LogManager.getLogger(ClientLauncher.class);
		//开始加载线程
		log.info("窗口加载线程开启");
		log.info("客户端加载线程开启");
		Thread t = new Thread(new PreLoad(),"PreLoad");
		t.start();
		BackInterface b = new Back();
		b.setType("load",false);
		log.info("等待加载线程结束...");
		while(!b.getType("load")){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				log.error("发生一个错误");
				log.error(e.getMessage());
				System.exit(0);
			}
		}
		b.delType("load_2");
		log.info("载入主界面");
		Windows.setWindow("main");
		Thread t11 = new Thread(new Windows(),"Windows");
		t11.start();
		log.info("预加载完毕,开始下一阶段加载");
		Thread t1 = new Thread(new NextLoad(),"NextLoad");
		t1.start();
		b.setType("load_2",false);
		log.info("等待加载线程结束...");
		while(!b.getType("load_2")){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				log.error("发生一个错误");
				log.error(e.getMessage());
				System.exit(0);
			}
		}

		/*File f = new File("version.txt");
		if(!(f.exists())) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}*/
		
		//Window.getupdateWindow();
		
		//test
		//PublicMsg.getPmToServer();
		//MainWindow.mw();
	}

}
