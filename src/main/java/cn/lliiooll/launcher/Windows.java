package cn.lliiooll.launcher;

import cn.lliiooll.launcher.window.LoadWindow;
import cn.lliiooll.launcher.window.MainWindow;
import cn.lliiooll.launcher.window.Window;

public class Windows implements Runnable{

    private static String name;

    @Override
    public void run() {
        if(getWindow() == "getupdate"){
            Window.getupdateWindow();
        }else{
            if(getWindow() == "update"){
                Window.updateWindow();
            }else{
                if(getWindow() == "updatecompelete"){
                    Window.updateCompeleteWindow();
                }else{
                    if(getWindow() == "load"){
                        new LoadWindow();
                    }else{
                        if(getWindow() == "main"){
                            MainWindow.mw();
                        }
                    }
                }
            }
        }
    }



    public static void setWindow(String name){
        Windows.name = name;
    }

    private String getWindow(){
        return name;
    }
}
