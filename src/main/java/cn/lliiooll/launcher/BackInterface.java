package cn.lliiooll.launcher;

public interface BackInterface {

    public void addType(String name,boolean type);

    public void setType(String name,boolean type);

    public boolean getType(String name);

    public void delType(String name);
}
